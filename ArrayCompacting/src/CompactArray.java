import java.lang.reflect.*;

	public class CompactArray<T> {
	   
	    public static void main(String[] args) {
	        Integer[] a1 = { 1, 2, 5, 6, 3, 8 };
	        // Show original
	        System.out.println(java.util.Arrays.toString(a1));
	        // Delete the element '5'
	        a1[2] = null;
	        a1 = (Integer[]) CompactArray.compact(a1);
	        System.out.println(java.util.Arrays.toString(a1));

	        Integer[] a2 = { 1, 2, 5, 6, 3, 8 };
	        // Remove first three elements
	        a2 = (Integer[]) CompactArray.compact(a2, new int[] { 0, 1, 2 });
	        System.out.println(java.util.Arrays.toString(a2));
	    }

	    /**
	     * Compact an array, removing elements
	     * whose indexes are listed in an
	     * array of indexes
	     *
	     * @param original The array to be compacted
	     * @param removals The indexes of the elements to remove
	     * 
	     * 
	     * private static StringBuilder appendRange(StringBuilder sb, int start, int previous) {
    sb.append(start);
    if(start!=previous) sb.append(previous-start>1? " - ": ", ").append(previous);
    return sb;
}
List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 12, 13, 14, 19);
StringBuilder sb = new StringBuilder();
int previous = list.get(0), start = previous;
for(int next: list.subList(1, list.size())) {
    if(previous+1 != next) {
        appendRange(sb, start, previous).append(", ");
        start = next;
    }
    previous = next;
}
String result = appendRange(sb, start, previous).toString();
	     * 
	     * 
	     *
	     * @return The compacted array
	     */
	    public static <T> T[] compact(T[] original, int[] removals) {
	        @SuppressWarnings("unchecked")
			T[] result = (T[]) Array.newInstance(original[0].getClass(),
	                original.length - removals.length);

	        int ixRemovals = 0;
	        int ixResult = 0;

	        for (int i = 0; i < original.length; i++) {
	            if ((ixRemovals < removals.length) && (i == removals[ixRemovals])) {
	                ixRemovals++;
	            } else {
	                result[ixResult++] = original[i];
	            }
	        }

	        return result;
	    }

	    /**
	     * Compact an array, 'removing' any element that
	     * is null
	     *
	     * @param original The array to be compacted
	     *
	     * @return The compacted array
	     */
	    public static <T> T[] compact(T[] original) {
	        T[] result = null;
	        int ix = 0;

	        for (int i = 0; i < original.length; i++) {
	            if (original[i] != null) {
	                original[ix++] = original[i];
	            }
	        }

	        if (ix != original.length) {
	            int i;

	            for (i = 0; (i < original.length) && (original[i] == null); i++) {
	            }

	            if (i == original.length) {
	                throw new RuntimeException(
	                    "All elements null. Cannot determine element type");
	            }

	            result = (T[]) Array.newInstance(original[i].getClass(), ix);
	            System.arraycopy(original, 0, result, 0, result.length);
	        }

	        return result;
	    }
	}
	
	
	
	/*
	 * String str = "This is 1 test 123-456-7890";

Pattern pattern = Pattern.compile("\\d{3}-\\d{3}-\\d{4}");
Matcher matcher = pattern.matcher(str);
if (matcher.find()) {
    System.out.println(matcher.group(0));
}
*/
	
	
	/*
	 *  public string CompactNumberRanges(IEnumerable<int> numbers, 
                                   int requiredRangeCount)
 {
     if (requiredRangeCount <= 1)
         throw new ArgumentOutOfRangeException("requiredRangeCount");

     int[] sorted = numbers.OrderBy(e => e).ToArray();

     StringBuilder b = new StringBuilder();

     for (int i = 0; i < sorted.Length; i++)
     {
         int cv = sorted[i];
         int count = 0;

         for (int j = cv; ; j++)
         {
             if (Array.IndexOf(sorted, j) == -1)
                 break;
             else
                 count++;
         }

         if (count == 0)
             throw new InvalidOperationException();
         else if (count < requiredRangeCount)
             b.Append(", ").Append(cv);
         else if (count >= requiredRangeCount)
         {
             b.Append(", ").AppendFormat("{0}-{1}", cv, sorted[i + count - 1]);

             i += count - 1;
         }
     }

     return b.ToString().Trim(',', ' '); ;
 }
 */


public class Anagram {
	
	public static boolean anagram(String s,String t) {
		if(s.length() > t.length()) {
			return false;
		}
		int[] letters = new int[256];
		int num_unique_chars = 0;
		int num_completed_t = 0;
		char[] char_array_s = s.toCharArray();
		for(char c : char_array_s) {
			if(letters[c]==0) {
				++num_unique_chars;
				++letters[c];
			}
		}
		for(int i=0;i<t.length();i++) {
			int c = (int) t.charAt(i);
			if(letters[c]==0) {
				return false;
			}
			--letters[c];
			if(letters[c]==0) {
				++num_completed_t;
				if(num_unique_chars == num_completed_t) {
					return i == t.length() - 1;
				}
			}
		}
		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "hi";
		String t = "ih";
		System.out.println(anagram(s,t));

	}

}

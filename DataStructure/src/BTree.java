
public class BTree {
	
	BTree left;
	BTree right;
	char data;
	
	public BTree() {
		left = right = null;
	}
	public void setLeft(BTree left) {
		this.left = left;
	}
	public BTree getLeft() {
		return left;
	}
	
	public void setRight(BTree right) {
		this.right = right;
	}
	public BTree getRight() {
		return right;
	}
	
	public void setData(char data) {
		this.data = data;
	}

}

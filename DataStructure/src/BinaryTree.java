
public class BinaryTree {
	
	public BinaryTree left;
	public BinaryTree right;
	public BinaryTree root;
	int data;
	
	public BinaryTree() {
		left = right = null;
	}
	
	public void setLeft(BinaryTree left) {
		this.left = left;
	}
	public BinaryTree getLeft() {
		return left;
	}
	public void setRight(BinaryTree right) {
		this.right = right;
	}
	public BinaryTree getRight() {
		return right;
	}
	public void setData(int data) {
		this.data = data;
	}
	public int getData() {
		return data;
	}
}

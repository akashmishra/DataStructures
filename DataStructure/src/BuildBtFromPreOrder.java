
public class BuildBtFromPreOrder {
	
	public BTree buildBinaryTree(char[] A,int i) {
		if(A == null) {
			return null;
		}
		if(A.length == i) {
			return null;
		}
		BTree newNode = new BTree();
		newNode.setData(A[i]);
		newNode.setLeft(null);
		newNode.setRight(null);
		
		if(A[i] == 'L') {
			return newNode;
		}
		i = i+1;
		newNode.setLeft(buildBinaryTree(A,i));
		i = i+1;
		newNode.setLeft(buildBinaryTree(A,i));
		return newNode;
	}
}

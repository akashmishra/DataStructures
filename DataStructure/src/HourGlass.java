import java.util.Scanner;

public class HourGlass {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int length = input.nextInt();
		int[][] a = new int[6][6];
		int max = Integer.MIN_VALUE;
		int temp;
		
		for(int i=0;i<6;i++) {
			for(int j=0;j<6;j++) {
				a[i][j] = input.nextInt();
			}
		}
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				
				temp = a[i][j] + a[i][j+1] + a[i][j+2]
					  + a[i+1][j+1]
					  + a[i+2][j] + a[i+2][j+1] + a[i+2][j+2];
				max = Math.max(temp, max);
				
			}
		}

		System.out.println(max);

	}

}

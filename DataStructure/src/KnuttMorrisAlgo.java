
public class KnuttMorrisAlgo {
	public static int F[] ;
	public static void prefixTable(int P[] , int m) {
		int i = 1, j = 0;
		F[0] = 0;
		while(i < m) {
			if(P[i] == P[j]) {
				F[i] = j+1;
				i++;
				j++;
			}
			else if(j > 0) {
				j = F[j-1];
			}
			else {
				F[i] = 0;
				i++;
			}
		}
		
	}
	
	public static int knottMorrisPlatt(int[] T,int n,int[] P,int m) {
		int i=0,j=0;
		prefixTable(P,m);
		
		while(i < m) {
			if(T[i] == P[j]) {
				if(j == m-1) {
					return i-j;
				}
				else {
					i++;
					j++;
				}
			}
			else if(j > 0) {
				 j = F[j-1];
			}
			else {
				i++;
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		int[] P = {1,2,3,4,5,6,7,8,9,10};
		int[] T = {1,2,3,4};
		int m = P.length;
		int n = T.length;
		int x = knottMorrisPlatt(T,n,P,m);
		System.out.print(x);
	}
	
}

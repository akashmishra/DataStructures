
public class LinkedListCircular {
	LinkedListNode head;
	
	public static class LinkedListNode {
		LinkedListNode next;
		int data;
		
		public LinkedListNode(int data) {
			this.data = data;
			next = null;
		}
	}
	
	public static LinkedListNode circularLinkedList(LinkedListNode head) {
		LinkedListNode n1 = head;
		LinkedListNode n2 = head;
		
		while(n2.next != null) {
			n1 = n1.next;
			n2 = n2.next.next;
			
			if(n1 == n2) {
				break;
			}
			
		}
		if(n2.next == null) {
		return null;
		}
		n1 = head;
		while(n1 != n2) {
			n1 = n1.next;
			n2 = n2.next;
		}
		return n2;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedListNode head = new LinkedListNode(10);
		LinkedListNode second = new LinkedListNode(11);
		LinkedListNode third = new LinkedListNode(10);
		LinkedListNode fourth = new LinkedListNode(13);
		
		head.next = second;
		second.next = third;
		third.next = fourth;
		
		System.out.println(circularLinkedList(head));

	}

}

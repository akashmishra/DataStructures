
public class LinkedListDeleteNodeInMiddle {
	LinkedListNode head;
	
	public static class LinkedListNode {
		LinkedListNode next;
		Object data;
		
		public LinkedListNode(Object data) {
			this.data = data;
			next = null;
		}
		
	}
	
	public static boolean deleteNodeInMiddle(LinkedListNode n) {
		if(n == null || n.next == null) {
			return false;
		}
		LinkedListNode next = n.next;
		n.data = next.data;
		n.next = next.next;
		return true;
		
	}
	
	public static void printLinkedList(LinkedListNode head) {
		LinkedListNode current = head;
		while(current != null) {
			System.out.print(current.data + "-->");
			current = current.next;
		}System.out.print(current);
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedListNode head = new LinkedListNode(10);
		LinkedListNode second = new LinkedListNode(11);
		LinkedListNode third = new LinkedListNode(12);
		LinkedListNode fourth = new LinkedListNode(13);
		LinkedListNode fifth = new LinkedListNode(14);
		
		head.next = second;
		second.next = third;
		third.next = fourth;
		fourth.next = fifth;
		
		deleteNodeInMiddle(third);
		printLinkedList(head);

	}

}

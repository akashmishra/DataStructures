
public class LinkedListNthNodeFromLast {
	LinkedListNode head;
	
	public static class LinkedListNode{
		LinkedListNode next;
		int data;
		
		LinkedListNode(int data){
			this.data = data;
			next = null;
		}
		
	}
	
	public static void nthFromLast(LinkedListNode head,int n) {
		LinkedListNode n1 = head;
		LinkedListNode n2 = head;
		
		for(int i=0;i<n-1;i++) {
			n1 = n1.next;
		}
		if(n1.next == null) {
			return ;
		}
		while(n1.next != null) {
			n1 = n1.next;
			n2 = n2.next;
		}
		System.out.print(n2.data);
	}
	
	public static void printLinkedList(LinkedListNode head) {
		LinkedListNode current = head;
		while(current != null) {
			System.out.print(current.data + "-->");
			current = current.next;
		}
		System.out.print(current);
	}
	
	public static void main(String[] args) {
		LinkedListNode head = new LinkedListNode(10);
		LinkedListNode second = new LinkedListNode(11);
		LinkedListNode third = new LinkedListNode(12);
		LinkedListNode fourth = new LinkedListNode(13);
		LinkedListNode fifth = new LinkedListNode(14);
		
		
		head.next = second;
		second.next = third;
		third.next = fourth;
		fourth.next = fifth;
		
		nthFromLast(head,3);
		
		
	}
	
}

public class LinkedListRemoveDuplicates {
	LinkedListNode head;
	
	public static class LinkedListNode {
		LinkedListNode next;
		int data;
		
		LinkedListNode(int data){
			this.data = data;
			next = null;
		}
	}
	public static void removeDuplicates(LinkedListNode head) {
		LinkedListNode previous = head;
		LinkedListNode current = previous.next;
		
		while(current != null) {
			LinkedListNode runner = head;
			while(runner != current) {
			if(runner.data == current.data) {
				LinkedListNode tmp = current.next;
				previous.next = tmp;
				current = tmp;
				break;
			}
			runner = runner.next;
		}
			if(runner == current) {
				previous = current;
				current = current.next;
			}
	}
	}
	public static void printLinkedList(LinkedListNode head) {
		LinkedListNode current = head;
		while(current != null) {
			System.out.print(current.data + "-->");
			current = current.next;
		}
		System.out.print(current);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedListNode head = new LinkedListNode(10);
		LinkedListNode second = new LinkedListNode(14);
		LinkedListNode third = new LinkedListNode(10);
		LinkedListNode fourth = new LinkedListNode(15);
		
		head.next = second;
		second.next = third;
		third.next = fourth;
		
		removeDuplicates(head);
		printLinkedList(head);

	}

}

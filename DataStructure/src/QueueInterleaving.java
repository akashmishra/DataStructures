import java.util.Queue;
import java.util.Stack;

public class QueueInterleaving {
	
	public static void interLeavingQueue(Queue<Integer> q) {
		if(q.size() % 2 != 0) {
			throw new IllegalArgumentException();
		}
		Stack<Integer> s = new Stack<Integer>();
		int halfSize = q.size() / 2;
		
		for(int i=0;i < halfSize;i++) {
			s.push(q.poll());
		}
		while(!s.isEmpty()) {
			q.offer(s.pop());
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

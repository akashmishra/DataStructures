import java.util.Queue;
import java.util.Stack;
import java.util.LinkedList;


public class QueueReversal {
	
	public static void reverseQueueFirstKElements(int k , Queue<Integer> q) {
		if(q== null || k > q.size()) {
			throw new IllegalArgumentException();
		}
		else if(k > 0) {
			Stack<Integer> s = new Stack<Integer>();
			for(int i=0;i< k;i++) {
			s.push(q.poll());
			}
			while(!s.isEmpty()) {
				q.offer(s.pop());
			}
			for(int i=0;i<q.size() - k;i++) {
				q.offer(q.poll());
			} 
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stack<Integer> s = new Stack<Integer>();
		Queue<Integer> q = new LinkedList<Integer>();
		
		q.offer(10);
		q.offer(20);
		q.offer(30);
		q.offer(40);
		q.offer(50);
		q.offer(60);
		q.offer(70);
		q.offer(80);
		q.offer(90);
		
		reverseQueueFirstKElements(4,q);
		System.out.print(q);
		

	}

}

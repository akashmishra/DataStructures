import java.util.*;
public class StackAscendingOrder {
	
	public static Stack<Integer> stackAscendingOrder(Stack<Integer> s) {
		Stack<Integer> r = new Stack<Integer>();
		
		while(!s.isEmpty()) {
			int tmp = s.pop();
			while(!r.isEmpty() && r.peek() > tmp) {
				s.push(r.pop());
			}
			r.push(tmp);
		}
		return r;	
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stack<Integer> s = new Stack<Integer>();
		s.push(100);
		s.push(0);
		s.push(14);
		s.push(88);
		
		System.out.println(stackAscendingOrder(s));

	}

}

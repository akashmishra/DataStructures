import java.util.Stack;

public class StackBalancedBracket {
	
	static Stack<Character> stk = new Stack<Character>();
	
	public static boolean isBalanced(String s) {
		
		if(s== null || s.length()==0) {
			return true;
		}
		
		for(int i=0;i<s.length();i++) {
			if(s.charAt(i) == '{') {
				if(!stk.isEmpty() && stk.peek() == '}') {
					stk.pop();
				}
				else {
					return false;
				}
			}
			else if(s.charAt(i) == '(') {
				if(!stk.isEmpty() && stk.peek() == ')') {
					stk.pop();
				}
				else {
					return false;
				}
			}else if(s.charAt(i) == '[') {
				if(!stk.isEmpty() && stk.peek() == ']') {
					stk.pop();
				}
				else {
					return false;
				}
			}
			else {
				stk.push(s.charAt(i));
			}
		}
			if(stk.isEmpty()) {
				return true;
			}
			else {
				return false;
			}
	
	}
	
	public static void main(String[] args) {
		String s = "{([])}";
		System.out.println(isBalanced(s));
	}

}

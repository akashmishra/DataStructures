import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;

public class StackConsecutiveData {
	
	public boolean checkStackPairWiseOrder(Stack<Integer> s) {
		Queue<Integer> q = new LinkedList<Integer>();
		boolean pairWiseOrdered = true;
		
		while(!s.isEmpty()) {
			q.add(s.pop());
		}
		while(!q.isEmpty()) {
			s.push(q.poll());
		}
		while(!s.isEmpty()) {
			int n = s.pop();
			q.offer(n);
			
			if(!s.isEmpty()) {
				int m = s.pop();
				q.offer(m);
				if(Math.abs(n-m) != 0) {
					pairWiseOrdered = false;
				}
			}
		}
		while(!q.isEmpty()) {
			s.push(q.remove());
			return pairWiseOrdered;
		}
		return pairWiseOrdered;
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StackConsecutiveData st = new StackConsecutiveData();
		Stack<Integer> s1 = new Stack<Integer>();
		s1.push(1);
		s1.push(3);
		s1.push(5);
		
		boolean a = st.checkStackPairWiseOrder(s1);
		System.out.println(a);

	}

}

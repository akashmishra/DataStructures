
public class StackFindingSpan {
	static int[] spans ;
	public static int[] findingSpan(int[] A) {
		int j,i;
		spans = new int[A.length];
		int span = 1;
		for(i=0;i<A.length;i++) {
			j=i-1;
			
			while(j >=0 && A[j]< A[j+1]) {
				span++;
				j--;
			}
			spans[i] = span;
			
		}
		return spans;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] A = {6,3,4,5,2};
		findingSpan(A);
		for(int s : spans) {
			System.out.println(s);
			
		}

	}

}

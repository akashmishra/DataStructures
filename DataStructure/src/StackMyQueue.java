import java.util.*;
public class StackMyQueue<T> {
    Stack<T> s1 ;
    Stack<T> s2 ;
    
    public StackMyQueue() {
    	s1 = new Stack<T>();
    	s2 = new Stack<T>();
    }
    
    public int size() {
    	return s1.size() + s2.size();
    }
    
    public void offer(T v) {
    	s1.push(v);
    }
    
    public T peek() {
    	if(!s2.isEmpty()) {
    		return s2.peek();
    	}
    	while(!s1.isEmpty()) {
    		s2.push(s1.pop());
    	}
    	return s2.peek();
    }
    public T remove() {
    	if(!s2.isEmpty()) {
    		s2.pop();
    	}
    	while(!s1.isEmpty()) {
    		s2.push(s1.pop());
    	}
    	return s2.pop();
    }
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StackMyQueue t = new StackMyQueue(); 
		
		for(int i=0;i<10;i++) {
			t.offer(i);
		}
		System.out.print(t.s1);

	}

}

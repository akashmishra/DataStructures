import java.util.Stack;

public class StackReverse {
	
	 static Stack s = new Stack();
	
	public static void insertAtBottom(char x) {
		if(s.isEmpty()) {
			s.push(x);
		}
		else {
			char a =(char) s.peek();
			s.pop();
			insertAtBottom(x);
			s.push(a);
		}
	}
	
	public static void reverse() {
		if(s.size() > 0) {
			char x = (char) s.peek();
			s.pop();
			reverse();
			insertAtBottom(x);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		s.push('1');
		s.push('2');
		s.push('3');
		s.push('4');
		
		System.out.println(s);
		
		reverse();
		
		System.out.print(s+" ");

	}

}

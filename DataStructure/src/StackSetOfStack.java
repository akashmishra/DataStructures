
class Node {
	Node above;
	Node below;
	int value;
	Node(int value){
		this.value = value;
	}
}
public class StackSetOfStack {
	
	int capacity;
	Node top;
	Node bottom;
	int size = 0;
	
	public StackSetOfStack(int capacity) {
		this.capacity = capacity;
	}
	public boolean isAtCapacity() {
		return capacity == size;
	}
	
	public void join(Node above , Node below) {
		if(below != null) {
		below.above = above;
	}
		if(above != null) {
			above.below = below;
		}
	}
	
	public boolean push(int v) {
		if(size >= capacity) {
			return false;
		}
		size++;
		Node n = new Node(v);
		if(size == 1) {
			bottom = n;
		}
		join(n,top);
		top = n;
		return true;
	}
	public int pop() {
		Node t = top;
		top = top.below;
		size--;
		return t.value;
	}
	public boolean isEmpty() {
		return size == 0;
	}
	public int removeBottom() {
		Node b = bottom;
		bottom = bottom.above;
		if(bottom != null) {
			bottom.below = null;
		}
		size--;
		return b.value;
	}
	
	public static void main(String[] args) {
		int capacit_per_stack = 5;
		StackSetOfStack s = new StackSetOfStack(capacit_per_stack);
		for(int i=0;i < 34;i++) {
			s.push(i);
		}
		for(int i=0;i<34;i++) {
			System.out.println(s.pop());
		}
		
	}
	
}


public class StackSingleArrayMultipleS {
	
	private int topOne;
	private int topTwo;
	private int size;
	private int[] A ;
	
	public StackSingleArrayMultipleS(int size) {
		this.size = size;
		topOne = -1;
		topTwo = this.size;
		A = new int[size];
	}
	public void push(int stackNum,int data) throws Exception {
		if(topTwo == topOne - 1) throw new Exception("stack is full");
		if(stackNum == 1) {
			
			A[++topOne] = data;
		}
		if(stackNum == 2) {
			A[size--] = data;
		}
	}
	
	public int pop(int stackNum) {
		if(stackNum == 1) {
			int data = A[topOne];
			topOne--;
			return data;
		}
		
		if(stackNum == 2) {
			int data = A[topTwo];
			topTwo++;
			return data;
		}
		return 0;
	}
	
	public boolean isEmpty(int stackNum) {
		if(stackNum == 1) {
			return topOne == -1;
		}
		else {
			return topTwo == size;
		}
	}
	
	
	
	

}

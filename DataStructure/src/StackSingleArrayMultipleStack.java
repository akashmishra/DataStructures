
public class StackSingleArrayMultipleStack {
	private static int stackSize = 300;
	private static int[] stackPointer = {0,0,0};
	private static int[] buffer = new int[stackSize * 3];

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		push(2,4);
		System.out.println(peek(2));
		push(0,3);
		push(0,5);
		push(0,7);
		System.out.println(peek(0));
		pop(0);
		System.out.println(peek(0));
	}
	
	public static void push(int stackNum , int value) {
		int index = stackNum * stackSize + stackPointer[stackNum]+1;
		stackPointer[stackNum]++;
		buffer[index] = value;
	}
	public static int pop(int stackNum) {
		int index = stackNum * stackSize + stackPointer[stackNum];
		stackPointer[stackNum]--;
		int value = buffer[index];
		buffer[index] = 0;
		return value;
	}
	
	public static int peek(int stackNum) {
		int index = stackNum * stackSize + stackPointer[stackNum];
		return buffer[index];
	}
	public static boolean isEmpty(int StackNum) {
		return stackPointer[StackNum] == StackNum * stackSize;
	}

}
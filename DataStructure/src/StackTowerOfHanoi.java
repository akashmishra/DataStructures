import java.util.*;
public class StackTowerOfHanoi {
	Stack<Integer> disks;
	int index;
	public StackTowerOfHanoi(int i){
		disks = new Stack<Integer>();
		index = i;
	}
	
	public int index() {
		return index;
	}
	
	public void add(int d) {
		if(!disks.isEmpty() && disks.peek() <= d) {
			System.out.println("Cannot add disks"+d);
		}else {
		disks.push(d);
	}
	}
	
	public void moveTopTo(StackTowerOfHanoi t) {
		int top = disks.pop();
		t.add(top);
		System.out.println(top +"Disk moved from " + index() +"to"+ t.index());
	}
	
	public void print() {
		System.out.println("Contents of tower"+index());
		for(int i=disks.size()-1;i>=0;i--) {
			System.out.println(" "+disks.get(i));
		}
	}
		public void moveDisks(int n,StackTowerOfHanoi destination,StackTowerOfHanoi buffer) {
			if(n > 0) {
				moveDisks(n-1,buffer,destination);
				moveTopTo(destination);
				moveDisks(n-1,destination,this);
			}
		}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 5;
		StackTowerOfHanoi[] tower = new StackTowerOfHanoi[n];
		for(int i=0;i<3;i++) {
			tower[i] = new StackTowerOfHanoi(i);
		}
		for(int i=n-1;i>=0;i--) {
			tower[0].add(i);
		}
		tower[0].moveDisks(n,tower[2],tower[1]);

	}
}

import java.util.*;
public class StackWithMin extends Stack<Integer> {
	
	 Stack<Integer> s2 ;
	
	public StackWithMin() {
		s2 = new Stack<Integer>();
	}
	
	public void push(int value) {
		if(value >= min() ) {
			s2.push(value);
		}
		super.push(value);
	}
	
	public Integer pop() {
		int value = super.pop();
		if(value == min()) {
			s2.pop();
		}
		return value;
	}
	public Integer min() {
		if(s2.isEmpty()) {
			return Integer.MAX_VALUE;
		}
		return s2.peek();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StackWithMin s = new StackWithMin();
		s.push(10);
		s.push(13);
		s.push(50);
		System.out.println(s.pop());
		System.out.println(s.min());

	}

}

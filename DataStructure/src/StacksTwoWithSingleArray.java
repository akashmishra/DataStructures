
public class StacksTwoWithSingleArray {
	
	private int topOne;
	private int topTwo;
	private int[] arrayStack;
	private int size;
	
	public StacksTwoWithSingleArray(int size) {
		this.size = size;
		topOne = -1;
		topTwo = size;
		arrayStack = new int[size];
	}
	
	public void push(int stackNum , int value) throws Exception {
		
			if(topTwo == topOne+1) throw new Exception("Stack is full");
			
			if(stackNum == 1) {
				arrayStack[++topOne] = value;
			}
			else if(stackNum == 2) {
				arrayStack[--topTwo] = value;
			}
			else {
				return ;
			}
		}
	
	public int pop(int stackNum) throws Exception {
		if(stackNum == 1) {
			if(topOne == -1) throw new Exception("Stack is empty");
			
			int toOne = arrayStack[topOne];
			arrayStack[topOne] = 0;
			topOne--;
			return toOne;
		}
		else if(stackNum == 2) {
			if(topTwo == this.size)throw new Exception("stack is empty"); 
			int toTwo = arrayStack[topTwo];
			arrayStack[topTwo++] =  0;
			return toTwo;
		}else {
			return 0;
		}
		
	}
	
	public int top(int stackNum) throws Exception {
		if(stackNum == 1) {
			if(topOne == -1) throw new Exception("Stack is empty");
			
			int toOne = arrayStack[topOne];
			return toOne;
		}
		else if(stackNum ==2) {
		if(topTwo == this.size) { throw new Exception("Stack is empty");}
		
		int toTwo = arrayStack[topTwo];
		return toTwo;
	}
	else {
		return 0;
	}
}
	public boolean isEmpty(int stackNum) {
		if(stackNum == 1) {
			return topOne == -1;
		}
		else if(stackNum == 2) {
			return topTwo == this.size;
		}
		else {
			return true;
		}
	}
	
	public static void main(String[] args) throws Exception {
		StacksTwoWithSingleArray st = new StacksTwoWithSingleArray(20);
		st.push(1, 10);
		st.push(2, 20);
		
		System.out.println(st.top(1));
		System.out.println(st.top(2));
		
		System.out.println(st.pop(1));
		System.out.println(st.pop(2));
		
	}
	
}

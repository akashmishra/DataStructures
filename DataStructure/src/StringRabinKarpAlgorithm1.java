
public class StringRabinKarpAlgorithm1 {
	public static final int d = 256;
	public void rabinKarp(String T , String P , int q) {
		int N = T.length();
		int M = P.length();
		int i,j;
		int p = 0;
		int t = 0;
		int h = 1;
		
		for(i = 0;i < M-1;i++) {
			h = (h*d)%q;
		}
		for(i=0;i< M;i++) {
			t = (d*t + T.charAt(i))%q;
			p = (d*p + P.charAt(i))%q;
		}
		for(i = 1;i <= N-M ;i++) {
			if(t == p) {
				for(j=1;j<=M;j++) {
					if(P.charAt(i) != T.charAt(i+j)) {
						break;
					}
				}
				if(j == M) {
					System.out.println("Patter found at index"+i);
				}
			}
			if(i < N-M) {
				t = (d*(t - T.charAt(i)*h) + T.charAt(i+M))%q;
				if(t < 0) t = t+q;
			}
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringRabinKarpAlgorithm1 r = new StringRabinKarpAlgorithm1();
		String txt = "Hey this is Akash";
		String pat = "Akash";
		int q = 101;
		r.rabinKarp(txt, pat, q);

	}

}

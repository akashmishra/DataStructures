
public class StringReverse {
	
	public static void reverseString(String s) {
		if(s == null) {
			return;
		}
		char[] temporary = s.toCharArray();
		int left , right =0;
		right =  temporary.length - 1;
		for(left=0;left < right;left++,right--) {
			char temp = temporary[left];
			temporary[left] = temporary[right];
			temporary[right] = temp;
		}
		for(char c : temporary) {
			System.out.print(c);
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method
		reverseString("akash");
	}

}


public class StringsBruteForceAlgorithm {
	
	public int bruteForceAlgo(int[] T,int[] P) {
		
		int lt = T.length;
		int lp = P.length;
		int max = lt - lp + 1;
		
		for(int i=1;i<=max;i++) {
			boolean flag = true;
			for(int j=1;j < lp && flag == true;j++) {
				if(P[j] != T[i+j-1]) {
					flag = false;
				}
			}
			if(flag == true) {
				return i;
			}
		}
		
		return -1;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] T = {1,2,3,4,5,6};
		int[] P = {5,6};
		
		StringsBruteForceAlgorithm bruh = new StringsBruteForceAlgorithm();
		int x = bruh.bruteForceAlgo(T, P);
		System.out.println(x);

	}

}

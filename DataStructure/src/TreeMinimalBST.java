class Tree{
	Tree left;
	Tree right;
	Tree parent;
	int data;
	Tree(int data){
		this.data = data;
	}
	public void setLeftChild(Tree left) {
		this.left = left;
		if(left != null) {
			left.parent = this;
		}
	}
	public void setRightChild(Tree right) {
		this.right = right;
		if(right != null) {
			right.parent = this;
		}
	}
}
public class TreeMinimalBST {
	public static TreeNode addToTree(int[] arr,int start,int end) {
		if(end < start) {
			return null;
		}
		int mid = (start + end)/2;
		TreeNode n = new TreeNode(arr[mid]);
		
		n.setLeftChild(addToTree(arr,start,mid-1));
		n.setRightChild(addToTree(arr,mid+1,end));
		
		return n;
	}
	public static TreeNode createMinimalBST(int[] array) {
		return addToTree(array,0,array.length-1);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {1,2,3,4,5,6,7,8,9,10};
		
		TreeNode root = createMinimalBST(arr);
		
		System.out.println(root.data);

	}

}

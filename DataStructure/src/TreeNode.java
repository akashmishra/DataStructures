
public class TreeNode {
	
	public int data;
	public TreeNode leftChild;
	public TreeNode rightChild;
	public TreeNode parent;
	
	public TreeNode(int data) {
		this.data = data;
	}
	
	public void setLeftChild(TreeNode leftChild) {
		this.leftChild = leftChild;
		if(leftChild != null) {
			leftChild.parent = this;
		}
	}
	
	public void setRightChild(TreeNode rightChild) {
		this.rightChild = rightChild;
		if(rightChild != null) {
			rightChild.parent = this;
		}
	}
	
	public int height() {
		int leftHeight = leftChild != null ? leftChild.height() : null;
		int rightHeight = rightChild != null ? rightChild.height() : null;
		
		return 1 + Math.max(leftHeight, rightHeight);
	
	}
    public int maxDepth(TreeNode root) {
    	if (root == null) {
    		return 0;
    	}
    	return 1 + Math.max(maxDepth(root.leftChild), maxDepth(root.rightChild));
    }
    
    public int minDepth(TreeNode root) {
    	if(root == null) {
    		return 0;
    	}
    	return 1 + Math.min(minDepth(leftChild), minDepth(rightChild));
    }
    public boolean checkBalancedTree(TreeNode root) {
    	return (maxDepth(root) - minDepth(root) <= 1);
    	}
}


public class Umpire {
	
	public int numFromGuesser;
	public int numFromPlayer1;
	public int numFromPlayer2;
	public int numFromPlayer3;
	
	public void collectNumFromGuesser() {
		Guesser g = new Guesser();
		numFromGuesser = g.guessNum();
	}
	
	public void numFromPlayers() {
		Player p1 = new Player();
		Player p2 = new Player();
		Player p3 = new Player();
		numFromPlayer1 = p1.guessNum();
		numFromPlayer2 = p2.guessNum();
		numFromPlayer3 = p3.guessNum();
	}
	
	public void compare() {
		if(numFromGuesser == numFromPlayer1) {
			System.out.println("Num from player 1 win");
		}else if(numFromGuesser == numFromPlayer2 ) {
			System.out.println("Num from Player 2 win");
		}else if(numFromGuesser == numFromPlayer3) {
			System.out.println("Player 3 wins" );
		}
		else {
			System.out.println("No one wins");
		}
	
	}

}

package com.hackerrank.java;

import java.util.Scanner;

public class DataTypes8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		int t = input.nextInt();
		
		for(int i=0;i<t;i++) {
			try {
			long n = input.nextInt();
			System.out.println("x can be fitted in");
				if(n>=-128 && n<= 127) {
					System.out.println("*byte");
				}
				if(n>=-32768 && n<= 32767) {
					System.out.println("*short");
				}
				if(n>= -2147483648 && n <= 2147483647) System.out.println("* int");
				if (n >= -9223372036854775808L && n <= 9223372036854775807L) System.out.println("* long");
				
			}
			catch(Exception e) {
				System.out.println("n can't be fitted any where");
			}
		}
		

	}

}

package com.hackerrank.java;

import java.util.Scanner;

public class HourGlass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		int max =0;
		int temp = 0;
		
		int[][] x = new int[6][6];
		
		for(int i=0;i<x.length;i++) {
			for(int j=0;j<x.length;j++) {
				x[i][j] = input.nextInt();
			}
		}
		
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				temp = x[i][j]+ x[i][j+1] + x[i][j+2]
						+ x[i+1][j+1]
					    + x[i+2][j] + x[i+2][j+1] +x[i+2][j+2];
				max = Math.max(max, temp);
			}
		}
		System.out.println(max);
	}

}

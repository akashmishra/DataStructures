package com.hackerrank.java;

import java.util.Scanner;

public class JavaForLoops7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		//int t = input.nextInt();
		
		for(int i=0;i<3;i++) {
			int a = input.nextInt();
			int b = input.nextInt();
			int n = input.nextInt();
			
			for(int j=0;j<n;j++) {
				a += b;
				if(j > 0) {
					System.out.print(" ");
				}
				System.out.print(a);
				
				b = b*2;
				
			}
			System.out.println();
		}

	}

}

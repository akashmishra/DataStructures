package com.hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormatting5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("=========================");
		for(int i=0;i<3;i++) {
			String s = input.next();
			int y = input.nextInt();
			
			System.out.printf("%-15s%03d%n",s,y);
		}
		
		System.out.println("=========================");

	}

}

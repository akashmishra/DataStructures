package com.hackerrank.java;

import java.util.Scanner;

public class JavaStdInputOp4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		
		int x = input.nextInt();
		double d = input.nextDouble();
		input.nextLine();
		String y = input.nextLine();
		
		System.out.println("String: "+y);
		System.out.println("Double "+d);
		System.out.println("Int "+x);

	}

}

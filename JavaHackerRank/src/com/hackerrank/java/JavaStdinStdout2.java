package com.hackerrank.java;

import java.util.Scanner;

public class JavaStdinStdout2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		String x = input.next();
		int y = input.nextInt();
		
		System.out.println("myString is "+x);
		System.out.println("myInt is "+y);

	}

}

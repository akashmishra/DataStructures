package com.hackerrank.java;

import java.util.Scanner;

public class StaticBlock {
	
	 private static int B;
	 private static int H;
	private static boolean flag = true;
	
	static {
		Scanner input = new Scanner(System.in);
		
		B = input.nextInt();
	    H = input.nextInt();
		
		input.close();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if(flag) {
			int area = B*H;
			System.out.println(area);
			
		}// end of if

	} // end of main

} // end of class

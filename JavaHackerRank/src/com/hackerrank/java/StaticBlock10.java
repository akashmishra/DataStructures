package com.hackerrank.java;

import java.util.Scanner;

public class StaticBlock10 {
	public static boolean flag = true;
	static int B;
	static int H;
	static {
		Scanner input = new Scanner(System.in);
		B =input.nextInt();
		H =input.nextInt();
		try {
			if(B<=0 || H <=0) {
				throw new Exception("Breadth and heigh must be positive");
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if(flag) {
			int area = B*H;
			System.out.println(area);
			
		}
		

	}

}

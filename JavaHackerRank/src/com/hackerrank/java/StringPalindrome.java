package com.hackerrank.java;

import java.util.Scanner;

public class StringPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		String s = input.next();
		
		String rev = "";
		
		for(int i=s.length()-1;i>=0;i--) {
			rev = rev + s.charAt(i);
		}
		if(s.equals(rev)) {
			System.out.println("Yes");
		}
		else {
			System.out.println("No");
		}

	}

}

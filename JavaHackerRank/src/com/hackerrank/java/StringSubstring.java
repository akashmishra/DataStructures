package com.hackerrank.java;

import java.util.Scanner;

public class StringSubstring {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in); 
		
		String s = input.next();
		
		int start = input.nextInt();
		int end = input.nextInt();
		
		System.out.println(s.substring(start, end));

	}

}


public class Factorial {
	
	public static long factorial(long n) {
		if(n <= 1) {
			return 1;
		}
		return n * factorial(n-1);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print(factorial(7));

	}

}

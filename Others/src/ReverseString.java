
public class ReverseString {
	
	public static void reverse(String s) {
		char[] temporary = s.toCharArray();
		int left , right = 0;
		
		right = temporary.length - 1;
		for(left = 0;left < right;left++,right--) {
			char temp = temporary[left];
			temporary[left] = temporary[right];
			temporary[right] = temp;
		}
		
		for(char x : temporary) {
			System.out.print(x);
		}
		
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String s = "akash";
		reverse(s);

	}

}

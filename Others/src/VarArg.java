
public class VarArg {
	
	public static void checkVarArg(int[]... x) {
		for(int[] x1 : x) {
			System.out.println(x1[0]);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] x= {{1,2,3},{4,5,6}};
		checkVarArg(x);
		
		int[] y = {1,2,3,4,5};
		checkVarArg(y);
		

	}

}


public class NumberPattern {
	
	public static void print(int n) {
		int k = 2*n - 2;
		char x = 'A';
		for(int i=0;i<n;i++) {
			
			for(int j=0;j<k;j++) {
				x = 'A';
				System.out.print(" ");
			}
			k = k-1;
			
			for(int l = 0;l<=i;l++) {
				System.out.print(x+" ");
				x++;
			}
			System.out.println();
		}
		
	}
	
	public static void main(String[] args) {
		print(6);
	}
	
}

class BubbleSort
{
    void bubbleSort(int arr[])
    {
    	int n = arr.length;
        for(int i=0;i<arr.length-1;i++) {
        	for(int j=i;j<n-i-1;j++) {
        		if(arr[j]>arr[j+1]) {
        			int temp = arr[j];
        			arr[j] = arr[j+1];
        			arr[j+1] = temp;
        		}
        	}
        }
    }
 
    /* Prints the array */
    void printArray(int arr[])
    {
      for(int i=0;i<arr.length;i++) {
    	  System.out.print(arr[i]+" ");
      }
      System.out.println();
    }
 
    // Driver method to test above
    public static void main(String args[])
    {
    	BubbleSort b = new BubbleSort();
    	int[] A= {86,54,71,60};
        b.bubbleSort(A);
        b.printArray(A);
    }
}


public class BubbleSort1 {
	
	public static void bubbleSort(int[] A) {
		
		int n = A.length;
		
		for(int i=0;i<n;i++) {
			for(int j=0;j<n-i-1;j++) {
				if(A[j] > A[j+1]) {
					int temp = A[j];
					A[j] = A[j+1];
					A[j+1] = temp;
				}
			}
		}
		for(int i=0;i<n;i++) {
			System.out.println(A[i]);
		}
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] A = {9,8,7,6};
		bubbleSort(A);

	}

}

public class InsertionSort{
	/*public void insertionSort(int[] A) {
		int i,j,v;
		for(i=2;i<A.length;i++) {
			v = A[i];
			j = i;
			while(A[j-1] > v && j >= 1) {
				A[j] = A[j-1];
				j--;
			}
			A[j] = v;
		}
	} */
	
	public void printArray(int[] A) {
		for(int i=0;i<A.length;i++) {
		System.out.print(A[i]+"");
	}
	}
	
	public static void main(String[] args) {
		InsertionSort i = new InsertionSort();
		int[] A = {20,80,11,71};
		i.insertionSort(A);
		i.printArray(A);
	}
	public void insertionSort(int[] A) {
		int i,j,v;
		for(i=1;i<A.length;i++) {
			v = A[i];
			j = i;
			
			while(A[j-1]>v && j>0) {
				A[j] = A[j-1];
				j--;
			}
			A[j] = v;
		}
	}
}

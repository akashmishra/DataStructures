
public class ReviseBubbleSort {
	
	public void bubbleSort(int[] A) {
		int n = A.length;
		for(int i=0;i<A.length;i++) {
			for(int j=0;j<n-i-1;j++) {
				if(A[j]>A[j+1]) {
					int temp = A[j];
					A[j] = A[j+1];
					A[j+1] = temp;
				}
			}
		}
	}
	
	public void print(int[] A) {
		for(int i=0;i<A.length;i++) {
			System.out.print(A[i]+"");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ReviseBubbleSort b = new ReviseBubbleSort();
		int[] A = {9,8,7,6};
		b.bubbleSort(A);
		b.print(A);
		
	}

}


public class ReviseInsertionSort {
	
	public void insertionSort(int[] A) {
		
		int i,j,v;
		int n = A.length;
		for(i=1;i<n;i++) {
			v = A[i];
			j=i;
			while(A[j-1]>v && j>0) {
				A[j] = A[j-1];
				j--;
			}
			A[j] = v;
		}
		
	}
	public void print(int[] A) {
		for(int i=0;i<A.length;i++) {
			System.out.print(A[i]+" ");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ReviseInsertionSort r = new ReviseInsertionSort();
		int[] A = {8,7,6,5};
		r.insertionSort(A);
		r.print(A);
		

	}

}

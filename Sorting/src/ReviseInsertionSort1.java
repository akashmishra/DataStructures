
public class ReviseInsertionSort1 {
	
	public static void insertionSort(int[] A) {
		int n = A.length;
		int i,j,v;
		for(i=2;i<n;i++) {
			v = A[i];
			j = i;
			while(A[j-1] > v && j>=1 ) {
				A[j] = A[j-1];
				j--;
			}
			A[j] = v;
		}
		
		for(int k=0;k<n;k++) {
			System.out.print(A[k]+" ");
		}
		
	}
	
	public static void main(String[] args) {
		int[] A = {9,8,7,6,5};
		insertionSort(A);
	}

}

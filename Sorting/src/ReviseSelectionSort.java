
public class ReviseSelectionSort {
	
	public void selectionSort(int[] A) {
		int i,j,min;
		int n = A.length;
		for(i=0;i<n;i++) {
			min = i;
			for(j=i+1;j<n;j++) {
				if(A[j]< A[min]) {
					min = j;
				}
			}
			int temp = A[i];
			A[i] = A[min];
			A[min] = temp;
		}
	}
	public void print(int[] A) {
		for(int i=0;i<A.length;i++) {
			System.out.print(A[i]+"");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ReviseSelectionSort s = new ReviseSelectionSort();
		int[] A = {5,4,3,2,1};
		s.selectionSort(A);
		s.print(A);
		
	}

}


public class Revision {
	
	public static void bubbleSort(int[] A) {
		System.out.println("Bubble Sort");
		int n = A.length;
		
		for(int i=0;i<n;i++) {
			for(int j=0;j<n-i-1;j++) {
				if(A[j] > A[j+1] ) {
					int temp = A[j];
					A[j] = A[j+1];
					A[j+1] = temp;
				}
			}
		}
	}
	
	public static void selectionSort(int[] A) {
		System.out.println("Selection Sort");
		int n = A.length;
		int min;
		
		for(int i=0;i<n-1;i++) {
			min = i;
			for(int j=i+1;j<n;j++) {
				if(A[j]< A[min]) {
					min = j;
				}
			}
			int temp = A[min];
			A[min] = A[i];
			A[i] = temp;
		}
	}
	
	public static void insertionSort(int[] A) {
		System.out.println("Insertion Sort");
		int n = A.length;
		
		int i,j,temp;
		
		for(i=2;i<n;i++) {
			temp = A[i];
			j = i;
			while(A[j-1] > A[j] && j>0) {
				A[j] = A[j-1];
				j--;
			}
			A[j] = temp;
		}
	}
	
	public static void shellSort(int[] A) {
		System.out.println("Shell Sort {9,8,7,6,5}");
		int n = A.length;
		
		int gap,i,j,temp;
		
		for(gap = n/2; gap>0; gap /= 2) {
			for(i= gap;i<n;i +=1) {
				temp = A[i];
				
				for(j=i; A[j-gap] > A[j] && j >= gap; j-= gap) {
					A[j] = A[j-gap];
				}
				A[j] = temp;
				
			}
		}
	}
	
	public static void print(int[] A) {
		for(int i=0;i<A.length;i++) {
			System.out.print(A[i]+" ");
			
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		int[] A = {9,8,7,6,5};
		bubbleSort(A);
		print(A);
		
		selectionSort(A);
		print(A);
		
		insertionSort(A);
		print(A);
		
		shellSort(A);
		print(A);
	}

}


public class SelectionSort {
	
	public static void selectionSort(int[] A) {
		int n = A.length;
		int i,j,min;
		for(i=0; i<n-1;i++) {
			min = i;
			
			for(j=i+1;j<n;j++) {
				if(A[j] < A[min]) {
					min = j;
				}
			}
			int temp = A[min];
			A[min] = A[i];
			A[i] = temp;
		}
		
		for(int k=0;i<A.length;k++) {
		System.out.print(A[k]+" ");
	}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] A = {7,6,5,4};
		selectionSort(A);

	}

}


public class ShellSort {
	
	public static void shellSort(int[] A) {
		
		int n = A.length;
		int temp,gap,i,j;
		for(gap = n/2; gap >= 0; gap /= 2) {
			
			for(i=gap;i<n;i++) {
				temp = A[i];
				
				for (j = i; j >= gap && A[j - gap] > temp; j -= gap) {
					A[j] = A[j-gap];
				}
					
					A[j] = temp;
				
			}
		}
		for(int k=0;k<n;k++) {
			System.out.print(A[k]+" ");
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] A = {9,8,7,6,5};
		shellSort(A);

	}

}

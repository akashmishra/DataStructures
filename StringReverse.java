
public class StringReverse {
	
	public static void stringReverse(String s) {
		if(s == null) {
			return;
		}
		int left , right = 0;
		char[] temporary = s.toCharArray();
		right = temporary.length - 1;
		for(left = 0;left < right ;left++,right--) {
			char temp = temporary[left];
			temporary[left]= temporary[right];
			temporary[right] = temp;
		}
		for(char t : temporary) {
			System.out.println(t);
		}
	}
	public static void main(String[] args) {
		StringReverse s = new StringReverse();
		s.stringReverse("akash");
	}

}